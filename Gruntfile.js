module.exports = function(grunt) {
	'use strict';

	require('matchdep')
		.filterDev('grunt-*')
		.forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		jsPolyfill: 'src/polyfill/*.js',
		jsSource: 'src/*.js',

		banner: 
			'/**\n' +
			' * corrector.js\n' +
			' * @author Alexander Burtsev, http://burtsev.me, <%= grunt.template.today("yyyy") %>\n' +
			' * @license MIT\n' +
			' */\n',

		jshint: {
			lib: ['<%= jsSource %>']
		},

		jscs: {
			options: {
				config: '.jscs.json'
			},
			lib: ['<%= jsSource %>']
		},

		concat: {
			options: {
				separator: '\n',
				banner: '<%= banner %>' + 
					';(function(window, document, undefined) {\n' +
					'\t\'use strict\';\n\n',
				footer: '\n})(window, document);'
			},
			lib: {
				src: ['<%= jsPolyfill %>', '<%= jsSource %>'],
				dest: 'dist/corrector.js'
			}
		},

		uglify: {
			options: {
				banner: '<%= banner %>'
			},
			lib: {
				files: {
					'dist/corrector.min.js': ['<%= concat.lib.dest %>']
				}
			}
		},

		watch: {
			lib: {
				files: ['<%= jsSource %>'],
				tasks: ['jshint', 'jscs', 'concat', 'uglify']
			}
		}
	});

	grunt.registerTask('default', ['jshint', 'jscs', 'concat', 'uglify', 'watch']);
};
