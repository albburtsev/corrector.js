	/**
	 * Entry point of corrector.js API
	 * @class
	 * @name Corrector
	 * @param {String} selector Selector of container with text
	 * @param {Object} rules Rules object
	 * @fires Corrector#corrector-start Fires before replacements
	 * @fires Corrector#corrector-end Fires after replacements
	 */
	function Corrector(selector, rules) {
		if ( !(this instanceof Corrector) ) {
			return new Corrector(selector, rules);
		}

		this.tree = Traversal(selector);
		this.rules = {};

		this
			.addRules(defaultRules)
			.addRules(rules);
	}

	Corrector.prototype = {
		/**
		 * Starts text replacements
		 * @returns {Corrector} Self object
		 */
		run: function() {
			var	_win = window.jQuery ? jQuery(window) : null,
				node, text,
				reNotWhipeSpace = /\S/;

			if ( _win ) {
				_win.trigger('corrector-start', this);
			}

			this.tree.reset();
			while ( (node = this.tree.next()) ) {
				if ( (text = node.nodeValue) && text.match(reNotWhipeSpace) ) {
					this.execute(node, text);
				}
			}

			if ( _win ) {
				_win.trigger('corrector-end', this);
			}

			return this;
		},

		/**
		 * Executes replacements for given text node
		 * @param {Object} node DOM text node (nodeType === 3)
		 * @param {Object} text Text of given node
		 * @returns {Corrector} Self object
		 */
		execute: function(node, text) {
			var rule;

			for (var name in this.rules) {
				if ( this.rules.hasOwnProperty(name) ) {
					rule = this.rules[name];
					if ( text.match(rule[0]) ) { // http://jsperf.com/match-vs-test
						node.nodeValue = text.replace(rule[0], rule[1]);
					}
				}
			}

			return this;
		},

		/**
		 * Adds custom rules to dictionary
		 * @param {Object} rules Rules object
		 * @returns {Corrector} Self object
		 */
		addRules: function(rules) {
			rules = rules || {};

			for (var name in rules) {
				if ( rules.hasOwnProperty(name) ) {
					this.rules[name] = rules[name];
				}
			}

			return this;
		},

		/**
		 * Clears dictionary with rules
		 * @param {Boolean} [useDefault=false] Use default rules if true
		 * @returns {Corrector} Self object
		 */
		clearRules: function(useDefault) {
			this.rules = useDefault ? defaultRules : {};
			return this;
		}
	};

	window.Corrector = Corrector;
