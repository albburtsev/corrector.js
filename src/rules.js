	/**
	 * Acknowledgements for Artem Sapegin
	 * Based on https://github.com/sapegin/richtypo.js/blob/master/rules/ru.js
	 */
	var	_nbsp = String.fromCharCode(160),
		_months = 'января|февраля|марта|апреля|мая|июня|июля|августа|сентября|ноября|декабря',
		_prepos = 'а|ай|в|вы|во|да|до|ее|её|ей|за|и|из|их|к|ко|мы|на|не|ни|но|ну|о|об|ой|он|от|по|с|со|то|ты|у|уж|я',
		_particles = 'б|бы|ж|же|ли|ль';

	var defaultRules = {
		_dashes: [/\s—/g, _nbsp + '—'],
		_months: [new RegExp('(\\d)\\s(' + _months + ')', 'gi'), '$1' + _nbsp + '$2'],
		_prepos: [new RegExp('([^а-яёА-ЯЁ]|^)(' + _prepos + ')\\s', 'gi'), '$1$2' + _nbsp],
		_particles: [new RegExp('\\s(' + _particles + ')(?![-а-яё])', 'gi'), _nbsp + '$1'],
	};
