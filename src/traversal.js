	/**
	 * Traverses all child nodes for given selector and saves text nodes
	 * @class
	 * @name Traversal
	 * @param {String} selector
	 */
	function Traversal(selector) {
		if ( !(this instanceof Traversal) ) {
			return new Traversal(selector);
		}

		var nodes, i;

		this.selector = selector || 'body';
		this.cursor = 0;
		this.storage = [];

		try {
			nodes = document.querySelectorAll(this.selector);
		} catch(e) {
			nodes = [];
		}

		for (i = nodes.length; i--;) {
			this.traverse(nodes[i]);
		}
	}

	Traversal.prototype = {
		/** @property {Array} List of excluded tags */
		exclude: [
			'script',
			'style',
			'iframe',
			'img',
			'input'
		],

		/**
		 * Recursive traverse of all child nodes
		 * @param {Object} node DOM-element
		 * @param {Number} [nesting=0] Nesting level
		 */
		traverse: function(node, nesting) {
			var nodeName = node.nodeName.toLowerCase(),
				nodeType = node.nodeType;

			if ( this.exclude.indexOf(nodeName) !== -1 ) {
				return;
			}

			nesting = nesting || 0;
			if ( node.nodeType === 1 ) {
				for (var nodes = node.childNodes, i = nodes.length; i--;) {
					this.traverse(nodes[i], nesting + 1);
				}
			} else if ( nodeType === 3 ) {
				this.storage.push(node);
			}
		},

		/**
		 * Iterator for found text nodes
		 * @returns {Object} Next text node
		 */
		next: function() {
			var current = this.storage[this.cursor];
			this.cursor++;
			return current;
		},

		/**
		 * Resets iterator
		 */
		reset: function() {
			this.cursor = 0;
		}
	};
